# Compress_Decompress
Build by **JetBrains PyCharm Community Edition 2019.1.1**

Python 3:

    numpy 1.16.3
    opencv-python 4.1.0.25
    prettytable 0.7.2
    matplotlib 3.0.3

### Algorithm use in this project and Reference

**[Huffman Coding][1]**

**[Run Length Coding][2]**

**[LZW Algorithm][3]**


[1]: http://pythonfiddle.com/huffman-coding-text/ "Huffman code" 

[2]: https://www.rosettacode.org/wiki/Run-length_encoding#Python "RLC"

[3]: https://rosettacode.org/wiki/LZW_compression#Python "LZW"

### Original Idea

From naive idea commpress character and after that commpress image
